import React from "react";
import "./header.css";
import { Layout as LayoutAntD } from "antd";
const { Header: HeaderAntD } = LayoutAntD;

export const Header = React.memo(() => (
  <HeaderAntD>
    <h1 className="header__logo">PodcastPlace</h1>
  </HeaderAntD>
));
