import React, {memo} from 'react';
import './footer.css';
import { Layout as LayoutAntD } from "antd";
const { Footer: FooterAntD } = LayoutAntD;

import Controls from '../../containers/Controls/Controls';

const footer = memo(() => (
    <FooterAntD>
        <Controls />
    </FooterAntD>
));

export default footer;