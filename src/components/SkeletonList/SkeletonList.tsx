import React, {memo} from 'react';
import { List, Skeleton } from "antd";

const skeletonList = memo(()=>(
    <List
          dataSource={new Array(10).fill(0)}
          renderItem={() => <Skeleton active />}
        />
));

export default skeletonList;