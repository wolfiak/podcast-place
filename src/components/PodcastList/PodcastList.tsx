import React, { Fragment, memo, FunctionComponent } from "react";
import { List, Collapse, Skeleton, Icon } from "antd";
import { Episode } from "../../resources/interfaces/episode.interface";
import { PodcastListProps } from "../../resources/interfaces/podcastListProps.interface";
const Panel = Collapse.Panel;

const iconStyles = {
  margin: "0 10px",
  fontSize: "22px",
  marginTop: "1px"
};

const listItemStyles = {
  alignItems: "center"
};

const podcastList: FunctionComponent<PodcastListProps> = memo((props: PodcastListProps) => (
  <Fragment>
    <List
      dataSource={props.episodes}
      loading={props.loading}
      size="large"
      renderItem={(episode: Episode) => (
        <List.Item style={listItemStyles}>
          <Icon style={iconStyles} type="play-circle" /> {episode.name}
        </List.Item>
      )}
    />
  </Fragment>
));

export default podcastList;
