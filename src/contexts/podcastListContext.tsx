import React, {
  Component,
  createContext,
  FunctionComponent,
  Suspense,
  lazy
} from "react";
import { Episode } from "../resources/interfaces/episode.interface";
import { Query } from "react-apollo";
import { getPodcast } from "../resources/quieries/getPodcast";
import SkeletonList from '../components/SkeletonList/SkeletonList';
 
const PodcastList = lazy(() => import("../components/PodcastList/PodcastList"));

const { Provider, Consumer } = createContext({});

interface PodcastListContextProps {}

interface PodcastListContextState {
  searchedPodcast: Episode[];
}

export interface PodcastListContext {
  searchedPodcast: Episode[];
  searchPodcast: (name: string) => FunctionComponent;
}
class PodcastListProvider extends Component<
  PodcastListContextProps,
  PodcastListContextState
> {
  constructor(props: PodcastListContextProps) {
    super(props);
    this.state = {
      searchedPodcast: []
    };
  }

  searchPodcast = (name: string) => {
    return (
      <Query query={getPodcast} variables={{ phrase: name }}>
        {({ data, loading }) => {
          if (!!data.podcast) {
            const { episodes } = data.podcast;

            return (
              <Suspense fallback={<p>Loading...</p>}>
                <PodcastList episodes={episodes} loading={loading} />
              </Suspense>
            );
          }

          return <SkeletonList />;
        }}
      </Query>
    );
  }

  render() {
    return (
      <Provider
        value={{
          searchedPodcast: this.state.searchedPodcast,
          searchPodcast: this.searchPodcast
        }}
      >
        {this.props.children}
      </Provider>
    );
  }
}

export { PodcastListProvider, Consumer as PodcastListConsumer };
