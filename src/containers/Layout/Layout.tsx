import React, { Fragment} from "react";
import { Layout as LayoutAntD } from "antd";
import "antd/dist/antd.css";
import { Header } from "../../components/Header/Header";
import Footer from '../../components/Footer/Footer';
import {PodcastListConsumer} from '../../contexts/podcastListContext';
const { Content : ContentAntD} = LayoutAntD;


export const Layout = () => (
  <Fragment>
    <LayoutAntD>
      <Header />
      <ContentAntD>
        <PodcastListConsumer >
          {(value: any)=> {
            console.log('Data:', value);
           console.log();
            return value.searchPodcast('Rozgrywka');
          }}
          </PodcastListConsumer>
      </ContentAntD>
      <Footer />
    </LayoutAntD>
  </Fragment>
);
