import React, { memo } from "react";
import "./controls.css";

const controls = memo(() => {
  return (
    <div className="controls__grid">
      <div className="controls__message">
        <p>Forumogadka</p>
        <p>Odcinek 123</p>
      </div>
      <div className="controls__progress-bar">
        <div>progress-bar</div>
      </div>
      <div className="controls__preview">
        <div>preview</div>
      </div>
      <div className="controls__start-stop">
        <div>start</div>
      </div>
      <div className="controls__next">
        <div>next</div>
      </div>
      <div className="controls__volume">
        <div>volume</div>
      </div>
    </div>
  );
});

export default controls;
