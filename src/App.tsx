import React, { Component } from 'react';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import { Layout } from './containers/Layout/Layout';
import {PodcastListProvider} from './contexts/podcastListContext';

const client = new ApolloClient({
  uri: "http://localhost:4000/graphql"
});

class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <PodcastListProvider>
        <Layout />
        </PodcastListProvider>
      </ApolloProvider>
    );
  }
}

export default App;
