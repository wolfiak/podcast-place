export interface Episode {
    id: string;
    name: string;
    link: string;
    __typename: string;
}
