import { Episode } from "./episode.interface";

export interface PodcastListProps{
    episodes: Episode[];
    loading: boolean;
}